import 'package:flutter/material.dart';

class CustomTextStyles {
		static const TextStyle title = TextStyle(
    	color: Colors.black,
    	fontSize: 18,
    	fontWeight: FontWeight.bold,	
	);

	static const TextStyle sectionTitle = TextStyle(
    	color: Colors.black,
    	fontSize: 18,
    	fontWeight: FontWeight.w600,	
	);

	static const TextStyle sectionName = TextStyle(
    	color: Colors.black,
    	fontSize: 16,
    	fontWeight: FontWeight.w500,	
	);

	static const TextStyle sectionInfo = TextStyle(
    	color: Color(0xff7B6F72),
    	fontSize: 13,
    	fontWeight: FontWeight.w400,
	);

	static const TextStyle categoryTitle = TextStyle(
    	color: Colors.black,
    	fontSize: 26,
    	fontWeight: FontWeight.w600,	
	);
}
