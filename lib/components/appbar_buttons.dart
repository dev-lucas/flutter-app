import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

/*------------------------------------------------------------------------------------------------------------------------------------*/
//MARK: Return Button

class ReturnButton extends StatelessWidget {
  	const ReturnButton({
  	    super.key,
  	});

  	@override
  	Widget build(BuildContext context) {
    	return Container(
            margin: const EdgeInsets.all(10),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: const Color(0xffF7F8F8),
                borderRadius: BorderRadius.circular(10)
            ),
            child: SvgPicture.asset(
                'assets/icons/Arrow - Left 2.svg',
                height: 20,
                width: 20,
            ),
    	);
  	}
}

/*------------------------------------------------------------------------------------------------------------------------------------*/
//MARK: Return Button Transparent

class ReturnTransparentButton extends StatelessWidget {
  	const ReturnTransparentButton({
  	    super.key,
  	});

  	@override
  	Widget build(BuildContext context) {
    	return Container(
            margin: const EdgeInsets.all(10),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: Colors.transparent,
            ),
            child: SvgPicture.asset(
                'assets/icons/Arrow - Left 2.svg',
                height: 20,
                width: 20,
            ),
    	);
  	}
}

/*------------------------------------------------------------------------------------------------------------------------------------*/
//MARK: Menu Button

class MenuButton extends StatelessWidget {
  	const MenuButton({
  	    super.key,
  	});

  	@override
  	Widget build(BuildContext context) {
    	return Container(
            margin: const EdgeInsets.all(10),
            alignment: Alignment.center,
            width: 37,
            decoration: BoxDecoration(
                color: const Color(0xffF7F8F8),
                borderRadius: BorderRadius.circular(10)
            ),
            child: SvgPicture.asset(
                'assets/icons/dots.svg',
                height: 5,
                width: 5,
            ),
    	);
  	}
}