import 'package:flutter/material.dart';
import '../../models/diet_model.dart';
import '../../pages/diet.dart';
import '../../utils/text_formatter.dart';
import '../../utils/text_style.dart';
import 'package:flutter_svg/svg.dart';

class Diets extends StatelessWidget {
  const Diets({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    List<DietModel> diets = DietModel.getDiets();

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.only(left: 20.0),
          child: Text("Recommendations\nfor Diet",
              style: CustomTextStyles.sectionTitle),
        ),
        const SizedBox(
          height: 15,
        ),
        SizedBox(
          height: 240,
          child: ListView.separated(
              separatorBuilder: (context, index) => const SizedBox(
                    width: 25,
                  ),
              itemCount: diets.length,
              scrollDirection: Axis.horizontal,
              padding: const EdgeInsets.only(left: 20, right: 20),
              itemBuilder: (context, index) {
                return GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                            builder: (context) => DietPage(diet: diets[index])),
                      );
                    },
                    child: Container(
                        width: 210,
                        decoration: BoxDecoration(
                            color: diets[index].boxColor.withOpacity(0.3),
                            borderRadius: BorderRadius.circular(20)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            SvgPicture.asset(diets[index].iconPath),
                            Column(children: [
                              Text(diets[index].name,
                                  style: CustomTextStyles.sectionName),
                              Text(
                                  TextFormatter.formatSectionInfo(
                                      diets[index].level,
                                      diets[index].duration,
                                      diets[index].calorie),
                                  style: CustomTextStyles.sectionInfo),
                            ]),
                            Container(
                              height: 45,
                              width: 130,
                              decoration: BoxDecoration(
                                  gradient: LinearGradient(colors: [
                                    diets[index].viewIsSelected ? const Color(0xff9DCEFF) : Colors.transparent,
                                    diets[index].viewIsSelected ? const Color(0xff92A3FD) : Colors.transparent,
                                  ]),
                                  borderRadius: BorderRadius.circular(50)),
                              child: Center(
                                child: Text(
                                  'View',
                                  style: TextStyle(
                                      color: diets[index].viewIsSelected ? Colors.white : const Color(0xffC58BF2),
                                      fontWeight: FontWeight.w600,
                                      fontSize: 14),
                                ),
                              ),
                            )
                          ],
                        )));
              }),
        )
      ],
    );
  }
}
