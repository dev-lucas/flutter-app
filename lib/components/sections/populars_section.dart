import 'package:flutter/material.dart';
import 'package:flutter_application_1/utils/text_formatter.dart';
import '../../models/popular_model.dart';
import '../../utils/text_style.dart';
import 'package:flutter_svg/svg.dart';

/*------------------------------------------------------------------------------------------------------------------------------------*/
//MARK: Populars

class Populars extends StatefulWidget {
  	const Populars({
  	  	Key? key,
  	}) : super(key: key);

	@override
  	_PopularsState createState() => _PopularsState();
}

class _PopularsState extends State<Populars> {
	List<PopularDietsModel> popularDiets = PopularDietsModel.getPopularDiets();
    
	@override
    Widget build(BuildContext context) {    
		return Column(
			crossAxisAlignment: CrossAxisAlignment.start,
			children: [
				const Padding(
				    padding: EdgeInsets.only(left: 20),
				    child: Text(
				  		'Populars',
				  		style: CustomTextStyles.sectionTitle
				    ),
				),

				const SizedBox(height: 15,), 

				ListView.separated(
					itemCount: popularDiets.length,
					shrinkWrap: true,//Necessario parar nao dar erro
					separatorBuilder: (context, index) => SizedBox(height: 15,),
					itemBuilder: (context, index) {
						return AnimatedContainer(
							duration: Duration(milliseconds: 250),
							height: 100,
							child: Row(
								mainAxisAlignment: MainAxisAlignment.spaceEvenly,
								children: [
									SvgPicture.asset(
										popularDiets[index].iconPath,
										width: 65,
										height: 65,
									),
									Column(
										mainAxisAlignment: MainAxisAlignment.center,
										crossAxisAlignment: CrossAxisAlignment.start,
										children: [
											Text(
												popularDiets[index].name,
												style: CustomTextStyles.sectionName
											),
											Text(
												TextFormatter.formatSectionInfo(popularDiets[index].level, popularDiets[index].duration, popularDiets[index].calorie),
												style: CustomTextStyles.sectionInfo
											),
										],
									),
									GestureDetector(
										onTap: () {
											setState(() {
												popularDiets.forEach((diet) {
													diet.boxIsSelected = false;
													if (diet.name == popularDiets[index].name) {
														diet.boxIsSelected = true;
													}
												});											  
											});
										},
										child: SvgPicture.asset(
											'assets/icons/button.svg',
											width: 30,
											height: 30,
										)
									)

								],
							),
							decoration: BoxDecoration(
								color: popularDiets[index].boxIsSelected ? Colors.white : Colors.white.withOpacity(0.001),
								borderRadius: BorderRadius.circular(16),
								boxShadow: [
									BoxShadow(
										color: popularDiets[index].boxIsSelected ? Color(0xff1D1617).withOpacity(0.1) : Colors.white.withOpacity(0.01),
										offset: Offset(0, 10),
										blurRadius: 40,
										spreadRadius: 0
									)
								]
							),
						);
					},
					padding: const EdgeInsets.only(
						left: 20,
						right: 20
					)
				)
			],
        );
    }
}