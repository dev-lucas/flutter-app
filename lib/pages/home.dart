import 'package:flutter/material.dart';
import 'package:flutter_application_1/components/appbar_buttons.dart';
import '../components/sections/category_section.dart';
import '../components/search_field.dart';

import '../components/sections/diet_sections.dart';
import '../components/sections/populars_section.dart';
import '../components/side_menu.dart';
import '../utils/text_style.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  //MARK: Main

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(),
      backgroundColor: Colors.white,
      body: ListView(
        children: const [
          SearchField(),
          SizedBox(height: 40),
          Categories(),
          SizedBox(height: 40),
          Diets(),
          SizedBox(height: 40),
          Populars()
        ],
      ),
    );
  }

  //MARK: App Bar

  AppBar appBar() {
    return AppBar(
      title: const Text('Breakfast', style: CustomTextStyles.title),
      backgroundColor: Colors.white,
      elevation: 0, //Remove shadow
      centerTitle: true,

      leading: GestureDetector(
          onTap: () {},
          child: const SideMenu()
      ),

      // actions: [
      //     GestureDetector(
      //         onDoubleTap: () {},
      //         child: MenuButton()
      //     )
      // ],
    );
  }
}

