import 'package:flutter/material.dart';
import 'package:flutter_application_1/utils/text_formatter.dart';
import 'package:flutter_svg/svg.dart';
import '../components/appbar_buttons.dart';
import '../models/diet_model.dart';
import '../utils/text_style.dart';

class DietPage extends StatefulWidget {
  final DietModel diet;

  const DietPage({super.key, required this.diet});

  @override
  State<DietPage> createState() => _DietPage();
}

class _DietPage extends State<DietPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          title: const Text("Diet", style: CustomTextStyles.sectionName),
          leading: GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: const ReturnTransparentButton()),
        ),
        extendBodyBehindAppBar: true,
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
            physics: const ClampingScrollPhysics(),
            child: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
              Container(
                margin: const EdgeInsets.all(8),
                height: 400,
                decoration: const BoxDecoration(
                  color: Color(0xfff2f2f2),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(50),
                    topRight: Radius.circular(50),
                  ),
                ),
                child: Padding(
                    padding: const EdgeInsets.only(
                      top: 140,
                      left: 60,
                      right: 60,
                      bottom: 30,
                    ),
                    child: Container(
                      decoration: BoxDecoration(boxShadow: [BoxShadow(color: Colors.white.withOpacity(0.6), blurRadius: 60, spreadRadius: 1.0)]),
                      child: SvgPicture.asset(widget.diet.iconPath),
                    )),
              ),
              Container(
                margin: const EdgeInsets.only(
                  top: 10,
                  left: 30,
                  right: 30,
                  bottom: 30,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.diet.name,
                      style: CustomTextStyles.categoryTitle,
                    ),
                    Text(
                      TextFormatter.formatSectionInfo(widget.diet.level, widget.diet.duration, widget.diet.calorie),
                      style: CustomTextStyles.sectionInfo,
                    )
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.only(
                  top: 10,
                  left: 30,
                  right: 30,
                  bottom: 30,
                ),
                child: const Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Details",
                      style: CustomTextStyles.sectionName,
                    ),
                    Text(
                      "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
                      style: CustomTextStyles.sectionInfo,
                    ),
                    Text(
                      "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
                      style: CustomTextStyles.sectionInfo,
                    )
                  ],
                ),
              ),
            ])));
  }
}
